Crear i validar documents RSS
=============================

MP4UF2A1T2

Estudi dels vocabularis RSS i Atom

Vocabularis RSS
---------------

Existeixen diferents [versions](aux/crono-rss.jpg) de RSS, sense comptar
el vocabulari Atom, que podem agrupar en dos branques (RDF –o RSS 1.\*–
i RSS 2.\*).

-   Vocabularis de la branca RDF: RSS 0.90 (RDF Site Summary, creat per
    Netscape), RSS 1.0 (encara RDF Site Summary), RSS 1.1 (pensat per
    substituir RSS 1.0).
-   Vocabularis de la branca RSS 2.\*: RSS 0.91 (Rich Site Summary,
    creat per Netscape), RSS 0.92, 0.93 i 0.94 (variacions de
    l’anterior), RSS 2.0.1 (Really Simple Sindication).
-   Vocabulari Atom: pensat per substituir tots els anteriors. Ben
    dissenyat, però amb dificultats per imposar-se i eliminar el
    caos existent.

Un dels participants en la definició del RSS 1.0, amb tan sols 14 anys
d’edat, va ser [Aaron
Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz#Life_and_works),
famós hacker i [activista](http://www.aaronsw.com/) per les llibertats a
Internet.

Validar documents RSS
---------------------

Donada la varietat de vocabularis RSS, la forma més convenient de
validar documents RSS és utilitzar el servei de validació del W3C.

-   [W3C Feed Validation Service](http://validator.w3.org/feed/)
-   [W3C Markup Validation Service](http://validator.w3.org/)
-   [W3C Quality Assurance Tools](http://www.w3.org/QA/Tools/)

Enllaços recomanats
-------------------

-   [Wikipedia: RSS](http://en.wikipedia.org/wiki/RSS)
-   [Wikipedia:
    Atom (standard)](http://en.wikipedia.org/wiki/Atom_(standard))
-   [Barrapunto](http://barrapunto.com/) ([*feed
    RDF*](http://barrapunto.com/index.rss))
-   [Slashdot](http://slashdot.org/) ([*feed* RSS
    2.0](http://rss.slashdot.org/Slashdot/slashdot))
-   [Bloc Pirata!](http://pirata.cat/bloc/) ([*feed* RSS
    0.92](http://pirata.cat/bloc/?feed=rss), [*feed* RSS
    2.0](http://pirata.cat/bloc/?feed=rss2), [*feed*
    Atom](http://pirata.cat/bloc/?feed=atom))

Pràctiques
----------

-   Fes per escrit una llista de diferències observables en els
    vocabularis dels diferents canals mencionats en els
    enllaços recomanats.
-   A partir del model d’Atom *feed* presentat al final de la pàgina
    crea, en un fitxer, un canal sobre un tema del teu interès, afegint
    un mínim de 3 `entry`. Valida el fitxer amb alguns dels serveis
    disponibles a Internet.
-   Verifica quins tipus MIME assigna el fitxer `/etc/mime.types` a les
    extensions `rss` i `xml`. Si l’extensió `xml` no te tipus MIME
    associat, associa-li `application/xml`; i si l’extensió `rss` no te
    tipus MIME associat, associa-li `application/rss+xml`.
-   Visualitza el fitxer amb Firefox i el lector Liferea. Influeix
    l’extensió del fitxer (prova `.xml` i `.rss`) en el comportament
    dels programes?

### Exemple de document Atom

Avis: podem crear UUIds (identificadors únics universals), i dates ben
formatades amb les ordres del shell, respectivament:

-   `uuidgen -t | sed 's/^/urn:uuid:/'`
-   `date -u --rfc-3339=seconds | sed 's/ /T/;s/+00:00$/Z/'`

Per calcular una data determinada passa a l’ordre `date` (seguint
l’exemple anterior) un argument extra amb la forma
`[MMDDhhmm[[CC]YY][.ss]]`.

    <?xml version="1.0" encoding="utf-8"?>
    <feed xmlns="http://www.w3.org/2005/Atom">
        <title>Example Feed</title>
        <subtitle>A subtitle.</subtitle>
        <link href="http://example.org/feed/" rel="self" />
        <link href="http://example.org/" />
        <id>urn:uuid:60a76c80-d399-11d9-b91C-0003939e0af6</id>
        <updated>2003-12-13T18:30:02Z</updated>
        <author>
            <name>John Doe</name>
            <email>johndoe@example.com</email>
        </author>

        <entry>
            <title>Atom-Powered Robots Run Amok</title>
            <link href="http://example.org/2003/12/13/atom03" />
            <link rel="alternate" type="text/html" href="http://example.org/2003/12/13/atom03.html"/>
            <link rel="edit" href="http://example.org/2003/12/13/atom03/edit"/>
            <id>urn:uuid:1225c695-cfb8-4ebb-aaaa-80da344efa6a</id>
            <updated>2003-12-13T18:30:02Z</updated>
            <summary>Some text.</summary>
        </entry>
    </feed>
            
