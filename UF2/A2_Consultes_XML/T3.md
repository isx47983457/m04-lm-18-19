Extreure dades de SGBD relacionals en format XML
================================================

MP4UF2A2T3

Tècniques tradicionals per convertir taules de SGBD relacionals a XML

L’ordre `psql`
--------------

No sempre cal disposar de facilitats primitives a un SGBDR per exportar
les seves dades a format XML. Una de les virtuts dels sistemes operatius
Posix és la possibilitat de combinar ordres existents en formes noves,
usant el shell i les tècniques de redirecció.

Facilitats proporcionades per l’ordre `psql`:

-   Es pot usar interactivament i en mode *batch* (per lots,
    no interactivament).
-   Pot llegir les ordres a executar d’un fitxer de text o
    l’entrada estàndard.
-   La majoria de les seves meta-ordres (les precedides per antibarra,
    com ara `\?`) tenen el seu equivalent en forma d’opcions de l’ordre
    `psql`.
-   La meta-ordre `\o` permet redirigir la sortida a fitxers o *pipes*.

PostgreSQL
----------

En aquesta pràctica instal·larem PostgreSQL si no ho has fet ja en
altres assignatures:

-   Instal·lació de PostgreSQL amb el sistema anterior a systemd:

        # yum install postgresql postgresql-server postgresql-devel
        # /sbin/service postgresql initdb     # no equivalent amb systemctl

        # chkconfig --list postgresql
        postgresql      0:off 1:off 2:off 3:off 4:off 5:off 6:off
        # chkconfig --levels 35 postgresql on   # systemctl enable postgresql 
        # chkconfig --list postgresql
        postgresql      0:off 1:off 2:off 3:on  4:off 5:on  6:off

-   (Opcional) Creació d'una base de dades i un usuari administrador de
    la mateixa de nom `training`:

        # /sbin/service postgresql start   # systemctl start postgresql 

        # su postgres
        bash-4.1$ createdb --encoding UTF-8 training 'Test Database'
        bash-4.1$ createuser --superuser --pwprompt training
        bash-4.1$ exit

        # vim /var/lib/pgsql/data/pg_hba.conf 
          Avís: AFEGIR AQUESTA LÍNIA ABANS ALTRES DECLARACIONS SIMILARS!!!
          host    training        training         127.0.0.1/32         md5

        # /sbin/service postgresql restart  # systemctl restart postgresql 
        # exit
        $ psql --host 127.0.0.1 --user training --dbname training

-   Per importar les dades inicials de la base de dades consulta a
    companys que ho hagin fet amb anterioritat.

La facilitat *doc-here* del shell
---------------------------------

Recorda: encara que pensada pel seu us dins de scripts, podem practicar
interactivament aquesta tècnica.

-   Si un script del shell ha de cridar a una ordre que necessita del
    seu propi fitxer d’ordres, ens trobem amb que hem de “mantenir” dos
    fitxers amb ordres.
-   Si posem el segon fitxer d’ordres dins del script del shell, el
    manteniment de la nova ordre és molt més simple.
-   A més a més, podem disposar de parts dinàmiques, calculades en tems
    d’execució, dins del segon fitxer (ara dins del script del shell).
-   La tècnica a usar per obtenir aquests beneficis és clàssica, pròpia
    del shell, i té per nom *doc-here*.

Exemple d’aquesta tècnica, i de com es va originar l’ordre `grep`:

    $ ed -s /etc/passwd <<-END
        g/^root/p
        q
    END

    root:x:0:0:root:/root:/bin/bash 

Enllaços recomanats
-------------------

-   Pàgina de manual de l’ordre `psql`(1).
-   Secció *Here Documents* de la pàgina de manual de l’ordre `bash`(1).
-   Altres tècniques: documentació del SGBDR Postgres, seccions [XML
    Type](http://www.postgresql.org/docs/9.0/static/datatype-xml.html) i
    [XML
    Functions](http://www.postgresql.org/docs/9.0/static/functions-xml.html).

Pràctiques
----------

-   Estudia la meta-ordre `\o` de l’ordre `psql`(1) (escrivint en un
    fitxer i també en un *pipe*).
-   De forma interactiva, i seguint les instruccions situades en el
    fitxer de l’ordre `psql2xml` (situada en el [repositori
    local](aux/psql2xml) de fitxers), exporta manualment una o dos
    taules de la base de dades *training* a format XML. Et caldrà
    practicar al mateix temps les dues pràctiques següents
    -   Explora, fins a entendre-les completament, les meta-ordres
        `\f '\t'`, `\a` i `\x` (i els seus equivalents en la
        línia d’ordres) de l’ordre `psql`(1) (manual: `man psql`; ajuda
        interactiva: \\?).
    -   Quina és la diferència entre les meta-ordres `\echo` i `\qecho`
        de l’ordre `psql`(1)?.
-   Fes un script del shell que utilitzant l’ordre `psql` i l’ordre
    `psql2xml` exporti alguna de les taules de la base de dades
    *training* a format XML. El script SQL a executar per l’ordre `psql`
    s’ha de llegir de l’entrada estàndard amb la tècnica del shell
    coneguda com *doc-here*.

