#!/usr/bin/python
#-*- coding: utf-8-*-

import sys
import cgi

# Debug
import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")
# Diccionari comunitats
comunitats = {
	'ES-AN':8379248,
	'ES-AR':1307984,
	'ES-AS':1028135, 
	'ES-CN':2126779,
	'ES-CB':580067,
	'ES-CM':2025510,
	'ES-CL':2407650,
	'ES-CT':7596131,
	'ES-EX':1072059,
	'ES-GA':2700970,
	'ES-IB':1128139,
	'ES-RI':315371,
	'ES-MD':6576009,
	'ES-MC':1477946,
	'ES-NC':647219,
	'ES-PV':2198657,
	'ES-VC':4959243
}

sortida = ''
totalHab = 0

# Llegim els valors

llista_codis=form.getlist("desplegable")


# Recorrem el diccionari per trobar la comunitat i mostrar els habitants
for codi in llista_codis:
	total = comunitats[codi]
	sortida = sortida + 'Habitants ' + codi + ' : ' + str(comunitats[codi]) + ' '
	totalHab += comunitats[codi]
			


# Mostrem la info
PAG= '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<body>
	<p> %s </p>
	<p> TOTAL FINAL: %s </p>
		
</body>
</html>
'''
write("\r\n")

write(PAG %(sortida,totalHab))

sys.exit(0)

