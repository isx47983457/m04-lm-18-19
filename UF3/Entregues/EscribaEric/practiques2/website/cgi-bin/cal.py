#!/usr/bin/python
#-*- coding: utf-8-*-

import sys
import cgi

# Debug
import cgitb; cgitb.enable()
resultat=0
write = sys.stdout.write
form = cgi.FieldStorage()


n1=form.getvalue("valor1")
n2=form.getvalue("valor2")
operacio=form.getvalue("operacions")
# COntrol Errors

if n1 is None:
	sys.stderr.write("No data")
	sys.stderr.write("\r\n")
	n1 = 0
	
if n2 is None:
	sys.stderr.write("No data")
	sys.stderr.write("\r\n")
	n2 = 0


try:
	num1 = float(n1)
	num2 = float(n2)
	
except ValueError:
	sys.stderr.write("Entrades no valides per a la calculadora.")
	sys.stderr.write("\r\n")


# Calculs

if operacio == 'suma':
	resultat = num1 + num2
elif operacio == 'resta':
	resultat = num1 - num2
elif operacio == 'mult':
	resultat = num1 * num2
elif operacio == 'divisio':
	if num2 != 0.0:
		resultat = num1 / num2
	else:
		resultat = 'ERROR, CANT DIVIDE BY ZERO'
else:
	# Ignorem entrada no valida i fem la suma
	resultat = num1 + num2


########## 		PROCESSAMENT RESPOSTA  ##############
	################################################
PAG = '''<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Resposta calculadora</title>
	</head>
	<body>
		<p> El resultat de %d %s %d es: %s </p>
		<br></br>
		<a href="/calculadora.html"> Calculadora </a>
	</body>
</html>
'''
write("Content-Type: text/html; charset=UTF-8\r\n") # or "text/html"...
write("\r\n")

write(PAG %(num1,operacio,num2,resultat))

sys.exit(0)

