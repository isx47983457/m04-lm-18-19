#!/usr/bin/python
#-*- coding: utf-8-*-

import sys
import cgi

# Debug
import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()

write("Content-Type: text/html; charset=UTF-8\r\n")
write("\r\n")

# No comprovo que la contrasenya actual sigui realment la actual
# Si comprovo que son diferents a l'actual i que sigui la mateixa dues vegades

# Passwords
apasswd = form.getvalue('oldpasswd')
newpasswd = form.getvalue('newpasswd')
renewpasswd = form.getvalue('renewpasswd')

# Sortida

deny1= '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<body>
	<p> Your password cant be the same as before</p>
	<a href=/chpasswd.html> Return to Lobby </a>		
</body>
</html>
'''

deny2= '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<body>
	<p> Your password does not match with the second one</p>
	<a href=/chpasswd.html> Return to Lobby </a>		
</body>
</html>
'''

access= '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<body>
	<p> Your password has been changed succesfully!</p>		
</body>
</html>
'''

# Comprovacio

if apasswd == newpasswd and apasswd == renewpasswd:
	write(deny1)
elif newpasswd != renewpasswd:
	write(deny2)
else:
	write(access)



sys.exit(0)
