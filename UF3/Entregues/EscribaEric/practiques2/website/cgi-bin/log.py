#!/usr/bin/python
#-*- coding: utf-8-*-


import sys
import cgi

# Debug
import cgitb; cgitb.enable()

write = sys.stdout.write

form = cgi.FieldStorage()

write("Content-Type: text/html; charset=UTF-8\r\n")
write("\r\n")


# body
# Mini Database per fer comprovacions (No és molt eficient però per l'exercici va be)
DB = { 'user01':'user01','pere':'pere','new':'old'}

#Extreiem dades
user = form.getvalue('login')
password = form.getvalue('passwd')

users = DB.keys()

# SORTIDES

deny= '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<body>
	<p> Invalid User or Password</p>
	<a href=/login.html> Return to Lobby </a>		
</body>
</html>
'''

access= '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
<body>
	<p> LOGIN SUCCESFULL!</p>		
</body>
</html>
'''


# Comprovacions

if user not in users:
	write(deny)
elif password != DB[user]:
	write(deny)
else:
	write(access)


sys.exit(0)
