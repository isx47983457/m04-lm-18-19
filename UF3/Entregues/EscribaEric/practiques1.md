# PRACTICA 1

## Eric Escriba


* Còpia el fitxer test.py en el directori cgi-bin i visita’l amb el navegador: què fa?


Si entrem amb la següent URL per visitar l'arxiu no ens apareix res.

```
http://localhost:8000/cgi-bin/test.py
```

Perquè ens aparegui algo necessitem passar-l'hi contingut com ara:

```
http://localhost:8000/cgi-bin/test.py?eric=xarli
```

Llavors ens retorna el següent: `eric ==> xarli`


* Verifica aquest script amb diferents navegadors:


### FIREFOX

Vist anteriorment:

```
http://localhost:8000/cgi-bin/test.py?eric=xarli
```

Ens retorna `eric ==> xarli`


### LYNX

Executem la següent ordre en consola:

```
lynx http://localhost:8000/cgi-bin/test.py?eric=xarli
```

I veiem que ens retorna exactament el mateix que amb el Firefox: `eric ==> xarli`


### ELINKS

És necessari passar-l'hi una URL començada per **http* ja que sino no funciona correctament.

És més **interactiu** que el lynx.

Executem **elinks** i quant ens demani la URL escribim el següent exemple:

```
http://localhost:8000/cgi-bin/test.py?eric=xarli&escriba=bum
```

Ens retorna el següent:

`escriba ==> bum`
`eric ==> xarli`


### CURL

És necessari enviar-l'hi la URL entre cometes quan estem tractant varis valors, 
ja que sinò no ens els retornarà tots.

L'executem de la següent forma:

```
curl "http://localhost:8000/cgi-bin/test.py?eric=xarli&new=one&abc=zyh"
```

Ens retorna: 

`new ==> one`
`abc ==> zyh`
`eric ==> xarli`


### WGET

L'executem de la següent forma (afegim la **opció -O** perque ens mostri per stdout i no ho guardi en cap file):

```
wget -O - http://localhost:8000/cgi-bin/test.py?prova=practiam04

--2019-02-26 09:49:37--  http://localhost:8000/cgi-bin/test.py?prova=practiam04
Resolving localhost (localhost)... ::1, 127.0.0.1
Connecting to localhost (localhost)|::1|:8000... failed: Connection refused.
Connecting to localhost (localhost)|127.0.0.1|:8000... connected.
HTTP request sent, awaiting response... 200 Script output follows
Length: unspecified [text/plain]
Saving to: ‘STDOUT’

-                         [<=>                                        ]       0  --.-KB/s               prova ==> practiam04
-                         [ <=>                                       ]      21  --.-KB/s    in 0s      

2019-02-26 09:49:37 (6.22 MB/s) - written to stdout [21]

```

* Executa aquest script directament en un terminal, definint les variables d’entorn necessàries.

Important passar-l'hi les variables entre cometes, ja que s'hi n'hi ha varies nomès en retornaria una.

```
python cgi-bin/test.py "eric=xarli&rober=wigga"
Content-Type: text/plain; charset=UTF-8

eric ==> xarli
rober ==> wigga
```

* Provoca en aquest script errors de sintaxi i errors en temps d’execució. Què passa?

Descomentar la següent linia perque em modul cgi funcioni:

`import cgitb; cgitb.enable()`


** ERRORS SINTAXI**

Basicament canviar el nom d'una variable i ja deixa de funcionar:

```
[isx47983457@i14 website]$ python cgi-bin/test.py "eeric=xarli"
Content-Type: text/plain; charset=UTF-8
<!--: spam
Content-Type: text/html

<body bgcolor="#f0f0f8"><font color="#f0f0f8" size="-5"> -->
<body bgcolor="#f0f0f8"><font color="#f0f0f8" size="-5"> --> -->
</font> </font> </font> </script> </object> </blockquote> </pre>
</table> </table> </table> </table> </table> </font> </font> </font><body bgcolor="#f0f0f8">
<table width="100%" cellspacing=0 cellpadding=2 border=0 summary="heading">
<tr bgcolor="#6622aa">
<td valign=bottom>&nbsp;<br>
<font color="#ffffff" face="helvetica, arial">&nbsp;<br><big><big><strong>&lt;type 'exceptions.NameError'&gt;</strong></big></big></font></td
><td align=right valign=bottom
><font color="#ffffff" face="helvetica, arial">Python 2.7.15: /usr/bin/python<br>Wed Feb 27 09:18:55 2019</font></td></tr></table>
    
<p>A problem occurred in a Python script.  Here is the sequence of
function calls leading up to the error, in the order they occurred.</p>
<table width="100%" cellspacing=0 cellpadding=0 border=0>
<tr><td bgcolor="#d8bbff"><big>&nbsp;</big><a href="file:///home/users/inf/hisx2/isx47983457/website/cgi-bin/test.py">/home/users/inf/hisx2/isx47983457/website/cgi-bin/test.py</a> in <strong><module></strong>()</td></tr>
<tr><td><font color="#909090"><tt>&nbsp;&nbsp;<small>&nbsp;&nbsp;&nbsp;16</small>&nbsp;#&nbsp;headers<br>
</tt></font></td></tr>
<tr><td><font color="#909090"><tt>&nbsp;&nbsp;<small>&nbsp;&nbsp;&nbsp;17</small>&nbsp;write("Content-Type:&nbsp;text/plain;&nbsp;charset=UTF-8\r\n")&nbsp;#&nbsp;or&nbsp;"text/html"...<br>
</tt></font></td></tr>
<tr><td bgcolor="#ffccee"><tt>=&gt;<small>&nbsp;&nbsp;&nbsp;18</small>&nbsp;a<br>
</tt></td></tr>
<tr><td><font color="#909090"><tt>&nbsp;&nbsp;<small>&nbsp;&nbsp;&nbsp;19</small>&nbsp;<br>
</tt></font></td></tr>
<tr><td><font color="#909090"><tt>&nbsp;&nbsp;<small>&nbsp;&nbsp;&nbsp;20</small>&nbsp;#&nbsp;body<br>
</tt></font></td></tr>
<tr><td><small><font color="#909090">a <em>undefined</em></font></small></td></tr></table><p><strong>&lt;type 'exceptions.NameError'&gt;</strong>: name 'a' is not defined
<br><tt><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>&nbsp;</tt>args&nbsp;=
("name 'a' is not defined",)
<br><tt><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>&nbsp;</tt>message&nbsp;=
"name 'a' is not defined"


<!-- The above is a description of an error in a Python program, formatted
     for a Web browser because the 'cgitb' module was enabled.  In case you
     are not reading this in a Web browser, here is the original traceback:

Traceback (most recent call last):
  File "cgi-bin/test.py", line 18, in &lt;module&gt;
    a
NameError: name 'a' is not defined

-->

```

** ERRORS EXECUCIÓ**

Introduim informació sense sentit:

```
[isx47983457@i14 website]$ python cgi-bin/test.py "eericd!xarli"
bash: !xarli: event not found

```

* Visita aquest script amb alguns dels formularis realitzats anteriorment.

En el formulari canviar la següent linia a:

```
<form action="cgi-bin/test.py" method="get">
```

Tenint en compte que el nom del formulari com a `index.html` simplement visitem la següent URL:

```
http://localhost:8000/
```

*EXEMPLE*

RETORN:

```
primerplat ==> Macarrons
postres ==> Pastís
textenviat ==> MOLT BO
segonplat ==> Carn
submit ==> Submit
cafe ==> cafesi
begudes ==> Aigua
```


* Estudia la documentació del mòdul cgi, especialment les propietats de l’objecte retornat per cgi.FieldStorage().


Guarada una **seqüència de camps**.

Aquesta classe proporciona **noms, escriptura, fitxers emmagatzemats al disc** i més. 
Al nivell superior, és accessible com un diccionari, el qual es claus són els noms de camp. (Nota: Cap pot passar com a nom del camp).
Els elements són una llista de Python (si hi ha diversos valors) o un altre objecte FieldStorage o MiniFieldStorage.

És a dir, ens retorna un diccionari amb claus valors per cada camp del formulari.

Té dues funcions bastant interessants que se li poden aplicar i ens serviran als exercicis següents que són: 

Exemple:

```
form = cgi.FieldStorage()

llista = comunitat = form.getlist("comunitat") # Retorna llista amb les comunitats
valor = form.getvalue("valor1") # Retorna el valor de l'element que te nom valor1

```


* Prepara un formulari i modifica aquest script per explorar el serveis proporcionats per aquest object.

A les pràctiques 2 queda aplicat aquest exercici.














